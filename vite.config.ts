import { fileURLToPath, URL } from 'node:url'
import { resolve } from 'path';
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import VuetifyPlugin from 'vite-plugin-vuetify'
import dotenv from 'dotenv';


const env = dotenv.config({ path: resolve(__dirname, '.env') }).parsed;



// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VuetifyPlugin(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  // Itère sur les clés et valeurs de 'env' , puis les assignes à "process.env"
  define: {
    'process.env': Object.entries(env || {}).reduce((prev, [key, value]) => {
      prev[key] = JSON.stringify(value);
      return prev;
    }, {} as Record<string, string>),
  },
})
