import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from './App.vue';
import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/styles'; 
import { createVuetify } from 'vuetify'; 
import
 { fr} from 'vuetify/locale';

// Créez l'instance de Pinia
const pinia = createPinia();

// Créez l'application Vue et utilisez Pinia comme plugin
const app = createApp(App);
app.use(pinia);


const defaultLocale: string = 'fr';
// Initialisez Vuetify et utilisez-le comme plugin avec createVuetify
const vuetify = createVuetify({
  locale: {
    locale: 'fr',
    messages: { fr },
  },
});

app.use(vuetify);

app.mount('#app');
