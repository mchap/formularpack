
/**
 * 
 * @param packs 
 * @returns 
 */
export const getTotalIndicators = (packs: any[]) => {
    return packs.reduce((total, pack) => total + pack.indics.length, 0);
  };
  
  /**
   * 
   * @param packs 
   * @param packIndex 
   * @param indicatorIndex 
   * @returns 
   */
export const getGlobalIndex = (
    packs: { indics?: any[] }[], 
    packIndex: number, 
    indicatorIndex: number
  ) => {
    if (packs.length === 0) {
      return 0;
    }
  
    let index = 0;
    for (let i = 0; i < packIndex && i < packs.length; i++) {
      if (packs[i] && packs[i].indics) {
        index += packs[i].indics.length;
      }
    }
    return index + indicatorIndex;
  };