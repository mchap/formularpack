
import { getTotalIndicators, getGlobalIndex } from '../Formular/utils';
import {describe, it,expect} from 'vitest'



describe('getTotalIndicators function', () => {
    it('returns the total number of indicators', () => {
      const packs = [
        { indics: [1, 2, 3] },
        { indics: [4, 5] },
      ];
      const result = getTotalIndicators(packs);
      expect(result).toBe(5);
    });
  
    it('returns 0 when packs array is empty', () => {
      const packs: any[] = [];
      const result = getTotalIndicators(packs);
      expect(result).toBe(0);
    });
  });
  
  describe('getGlobalIndex function', () => {
    it('calculates the correct global index', () => {
      const packs = [
        { indics: [1, 2, 3] },
        { indics: [4, 5] },
      ];
      const result = getGlobalIndex(packs, 1, 1);
      expect(result).toBe(4);
    });
  
    it('returns 0 when packs array is empty', () => {
      const packs: any[] = [];
      const result = getGlobalIndex(packs, 1, 1);
      expect(result).toBe(0);
    });
  
    it('returns 0 when packIndex and indicatorIndex are 0', () => {
      const packs = [
        { indics: [1, 2, 3] },
        { indics: [4, 5] },
      ];
      const result = getGlobalIndex(packs, 0, 0);
      expect(result).toBe(0);
    });
  });