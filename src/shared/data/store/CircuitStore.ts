import axios, { type AxiosRequestConfig } from "axios";
import { defineStore } from "pinia";
import { API_CONFIG} from '../../../../config';

interface CircuitState  {
    //A CHANGER utiliser l'interface
    circuits: never []
}

export const useCircuitState = defineStore('CircuitStore', {

    state: (): CircuitState => ({
        circuits: [],
    }),

    actions: {
        async fetchCircuits(poolId: any) {
            try {
                const {token} = API_CONFIG;  

                 // Ajouter le token dans le header de la requête
                const config: AxiosRequestConfig = {
                    headers: {
                        Authorization: `Bearer ${token}`,
            },
          };

          const response = 
          await 
          axios.get(`http://127.0.0.1/api/v1/circuits?page=1&pool.id=${poolId}`, config);
          this.setCircuits(response.data);
        //   console.log(response.data);

            }

            catch (error) {
                console.error('Erreur lors de la récupération des indicators depuis l\'API', error);
              }
        },

        // Mutation
    setCircuits(circuits: never[]) {
        this.circuits = circuits;
      },
    }


})