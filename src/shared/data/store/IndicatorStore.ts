import axios, { type AxiosRequestConfig } from "axios";
import { defineStore } from "pinia";
import IndicatorService from "../services/IndicatorService";
import type { Indicator } from "../../interfaces/Indicator.interface";
import type {Pack} from "../../interfaces/Pack.interface";
import type {CircuitIndicator} from "../../interfaces/CircuitIndicator.interface";
import type {PondIndicator} from "../../interfaces/PondIndicator.interface";
import { API_CONFIG} from '../../../../config';





interface IndicatorState {
  indicators: Indicator[];
  packs: Pack[];
  packsCircuits: CircuitIndicator[];
  packsPonds: PondIndicator[];
}


export const useIndicatorState = defineStore("IndicatorStore", {
    //state
    state: (): IndicatorState => ({
        indicators: [],
        packs: [],
        packsCircuits: [],
        packsPonds: []
    }),

     /**
      * BY POOL
      * Récuperer les indicators par Piscine
      */
  actions: {

    /**
     * 
     * @param poolId 
     */
    async fetchIndicatorsByPoolId(poolId: number) {

    
        try {
         
          const {token} = API_CONFIG;  
          
          // Ajouter le token dans le header de la requête
          const config: AxiosRequestConfig = {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          };

          const packs: Pack[] = [];
          const response = await 
          axios.get<any[]>
          (`http://127.0.0.1/api/v1/indicators?page=1&pools.id=${poolId}`, config);
       
          const indicators: Indicator[] = response.data.map(IndicatorService.mapToIndicator);

          indicators.forEach(indicator => {
              const categoryName = IndicatorService.getCategoryName(indicator.measure_pack_category);
              if (categoryName) {
                  const existingPack = packs.find(pack => pack.packName === categoryName);

                  if (existingPack) {
                      existingPack.indics.push(indicator);
                  } else {
                      packs.push({ 
                        packName: categoryName, 
                        indics: [indicator], 
                        isGeneral:true, 
                        isCircuit:false, 
                        isPond: false });
                  }
              }
          });

          // Maintenant, packs contient les packs basés sur les catégories des indicateurs
          console.log(packs);

          // Mise à jour de l'état avec les packs créés
          this.setPacks(packs);
      } catch (error) {
          console.error('Erreur lors de la récupération des indicateurs depuis l\'API', error);
      }
  },

  /**
   * 
   * @param circuitId 
   * @param circuitNameFromView 
   * @returns 
   */
   async fetchIndicatorsByCircuit(circuitId: any, circuitNameFromView: string){
    try {
        const {token} = API_CONFIG;  
        const config: AxiosRequestConfig = {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };

        const packsCircuits: CircuitIndicator[] = [];

        const response = await axios.get<any[]>(`http://127.0.0.1/api/v1/indicators?page=1&circuits.id=${circuitId}`, config);
        const indicators: Indicator[] = response.data.map(IndicatorService.mapToIndicator);

        indicators.forEach(indicator => {
            const categoryName = IndicatorService.getCategoryName(indicator.measure_pack_category);
            if (categoryName) {
                const existingPack = packsCircuits.find(pack => pack.packName === categoryName);

                if (existingPack) {
                    existingPack.indics.push(indicator);
                } else {
                    packsCircuits.push({ 
                        circuitName: circuitNameFromView, 
                        packName: categoryName, 
                        indics: [indicator],
                        isGeneral: false, 
                        isCircuit: true, 
                        isPond: false  
                    });
                }
            }
        });

        // console.log('Réponse de l\'API pour le circuit', circuitId, ' : ', packsCircuits);
        return packsCircuits;

    } catch(error) {
        console.error('Erreur lors de la récupération des indicateurs depuis l\'API', error);
        throw error;
    }
},


   /**
      * BY POND
      * Récuperer les indicators par POND
      */
   async fetchIndicatorsByPond(pondId: any, pondNameFromView: string){
    try {
        const {token} = API_CONFIG;  
        const config: AxiosRequestConfig = {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };

        const packsPonds: PondIndicator[] = [];

        const response = await axios.get<any[]> (`http://127.0.0.1/api/v1/indicators?page=1&ponds.localId=${pondId}`, config);
        const indicators: Indicator[] = response.data.map(IndicatorService.mapToIndicator);

        indicators.forEach(indicator => {
            const categoryName = IndicatorService.getCategoryName(indicator.measure_pack_category);
            if (categoryName) {
                const existingPack = packsPonds.find(pack => pack.packName === categoryName);

                if (existingPack) {
                    existingPack.indics.push(indicator);
                } else {
                  packsPonds.push({ 
                        pondName: pondNameFromView, 
                        packName: categoryName, 
                        indics: [indicator],
                        isGeneral: false, 
                        isCircuit: false, 
                        isPond: true  
                    });
                }
            }
        });

        // console.log('Réponse de l\'API pour le circuit', pondId, ' : ', packsPonds);
        return packsPonds;

    } catch(error) {
        console.error('Erreur lors de la récupération des indicateurs depuis l\'API', error);
        throw error;
    }
},


  setPacks(packs: Pack[]) {
    this.packs = packs;
    },
  },

})
