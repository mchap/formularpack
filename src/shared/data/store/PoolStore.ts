import axios, { type AxiosRequestConfig } from "axios";
import { defineStore } from "pinia";
import { API_CONFIG} from '../../../../config';

interface PoolState {
  pools: never[];
}


export const usePoolStore = defineStore("PoolStore", {
  // State
  state: (): PoolState => ({
    pools: [],
  }),

  // Actions
  actions: {
    async fetchPools() {
        try {
          /**
           *   Récuperer la propriété à l'objet API_CONFIG, pour y acceder il faut le destructurer ou y acceder via
          la propriété 'token'
           */
          const {token} = API_CONFIG; 
        
          // Ajouter le token dans le header de la requête
          const config: AxiosRequestConfig = {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          };
  
          const response = await axios.get(`http://127.0.0.1/api/v1/pools?page=1`, config);
          
          
          this.setPools(response.data);


        } catch (error) {
          console.error('Erreur lors de la récupération des piscines depuis l\'API', error);
        }
      },

    // Mutation
    setPools(pools: never[]) {
      this.pools = pools;
    },
  },
});
