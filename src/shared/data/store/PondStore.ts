import axios, { type AxiosRequestConfig } from "axios";
import { defineStore } from "pinia";
import { API_CONFIG} from '../../../../config';


interface PondState {
    //ne peut pas être instancié
    ponds: never []
}

export const usePondState = defineStore('PondStore', {

    state: (): PondState => ({
        ponds: []
    }),

actions: {
    async fetchPonds(poolId: any) {
        try{

            const {token} = API_CONFIG;  
            // Ajouter le token dans le header de la requête
           const config: AxiosRequestConfig = {
               headers: {
                   Authorization: `Bearer ${token}`,
       },
     };

     const response = 
     await
     axios.get(`http://127.0.0.1/api/v1/ponds?page=1&pool=${poolId}`, config);
     this.setPonds(response.data);
    //  console.log(response.data);

        } catch (error) {
            console.log(error);
            
        }
    },

    setPonds(ponds: never[]) {
        this.ponds = ponds;
    }
}
})