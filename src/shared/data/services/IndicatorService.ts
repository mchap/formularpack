
import type { Indicator } from "../../interfaces/Indicator.interface";

export default class IndicatorService {
    static categories: Record<number, string> = {
        0: 'Fréquentation',
        1: 'Réglementation',
        2: 'Eau',
        3: 'Energie',
        4: 'Air',
        5: 'Chemical',
        6: 'StringDatum',
        7: 'DatasetDatum',
        8: 'IndexMeasure',
        9: 'Financier',
        // Ajoute d'autres correspondances si nécessaire
    };
    
     // Méthode pour récupérer le nom de la catégorie à partir de son identifiant
      static getCategoryName(categoryId: number): string | undefined {
        return this.categories[categoryId];
    }
    static mapToIndicator(rawData: any): Indicator {
        // Transformer les données brutes de l'API pour correspondre à la structure de l'interface Indicator
        return {
            id: rawData.id,
            label: rawData.label,
            measure_Id: rawData.measure.id,
            measure_pack_category: rawData.measure.category,
            unit: rawData.unit
            // ...
        };
    }
}
