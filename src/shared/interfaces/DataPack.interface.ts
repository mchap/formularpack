
export interface DataPack {
   
    value: number
    ref_measure_id: number
    pondId?: number
    circuitId?: number
    poolId?: number
    datetime: Date
    date: Date

}