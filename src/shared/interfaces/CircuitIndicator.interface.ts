import type { Pack } from "./Pack.interface"

export interface CircuitIndicator extends Pack{

    circuitName:string
 
}

export default CircuitIndicator;