import type { Pack} from "./Pack.interface";

export interface PondIndicator extends Pack {
    pondName:string
}

export default PondIndicator;