export interface Indicator {
    id: number;
    label: string;
    measure_Id: number;
    measure_pack_category: number;
    unit: string;

}